
\chapter{Introducción}


\lettrine[lraise=0.1, findent=2pt]{E}{ste} libro es sobre la demostración de teoremas.
Hasta este momento en tu educación, las matemáticas han sido probablemente
presentadas como una disciplina primariamente computacional. Has aprendido como
solucionar ecuaciones, calcular derivadas e integrales, multiplicar matrices y 
encontrar determinantes; y has presenciado cómo estas cosas pueden responder 
preguntas prácticas en el mundo real.

Pero existe otro lado de las matemáticas que es más teórico que computacional.
Aquí la meta primaria es entender estructuras matemáticas, comprobar sentencias
matemáticas, y hasta inventar o descubrir nuevos teoremas y teorías matemáticas.
La técnicas y procedimientos matemáticos que has aprendido
y utilizado hasta ahora están basados en el lado teórico de las matemáticas. Por
ejemplo, al calcular el área debajo de una curva, utilizas el teorema fundamental 
del cálculo. Es debido a que este teorema es verdadero que tu respuesta es
correcta. Sin embargo, al aprender cálculo estabas más preocupado con cómo
aplicar el teorema que con entender la razón por la cual es verdadero. ¿Pero cómo
sabemos que es verdadero? ¿Cómo nos convencemos a nosotros mismos o a otros de
su validez? Preguntas de esta índole pertenecen al campo teórico de las
matemáticas. Este libro es una introducción a este campo. 

Este libro te introducirá a un mundo esotérico. Aprenderás y aplicarás los
métodos de razonamiento que los matemáticos usan para verificar teoremas, 
explorar verdades matemáticas y crear nuevas teorías matemáticas. Esto te
prepará para cursos avanzados de matemáticas, debido a que serás capaz de
entender las demostraciones, escribir tus propias demostraciones y pensar
crítica e inquisitivamente sobre las matemáticas.

El libro estás organizado en cuatro partes, como se detalla más abajo.

\vspace{0.2in}
\textbf{PARTE I Fundamentos}
\begin{itemize}
\item Capítulo 1: Conjuntos
\item Capítulo 2: Lógica
\item Capítulo 3: Conteo
\end{itemize}
\vspace{0.1in}

Los Capítulos 1 y 2 establece el lenguaje y las convenciones usadas en todas las matemáticas avanzadas. Los conjuntos son fundamentales porque cada estructura matemática, objeto, o entidad puede ser descrita como un conjunto. La lógica es fundamental porque nos permite entender el significado de sentencias, para deducir hechos sobre estructuras matemáticas y descubrir estructuras adicionales. Todos los capítulos subsiguientes edifican sobre estos dos primeros capítulos. El Capítulo 3 se incluye parcialmente porque sus temas son centrales en muchas ramas de las matemáticas, pero también porque es una fuente de muchos ejemplos y ejercicios que ocurren a lo largo del libro. (Sin embargo, el instructor del curso puede omitir el Capítulo 3.)

\vspace{0.2in}
\textbf{PARTE II Demostrando Sentencias Condicionales}
\begin{itemize}
\item Capítulo 4: Demostración Directa
\item Capítulo 5: Demostración Contrapositiva
\item Capítulo 6: Demostración por Contradicción
\end{itemize}

\vspace{0.1in}
Los Capítulos 4-6 tratan con tres técnicas principales usadas para demostrar teoremas que tienen la forma "condicional" "\textit{Si $ P $, entonces $ Q $}".

\vspace{0.2in}
\textbf{PARTE III Más sobre Demostraciones}
\begin{itemize}
\item Capítulo 7: Demostrando Sentencias que no son Condicionales
\item Capítulo 8: Demostraciones que Involucran Conjuntos
\item Capítulo 9: Refutación
\item Capítulo 10: Inducción Matemática
\end{itemize}
\vspace{0.1in}

Estos capítulos tratan con variaciones, embellecimientos y consecuencias útiles de las técnicas de demostración en los Capítulos 4-6.

\vspace{0.2in}
\textbf{PARTE IV Relaciones, Funciones y Cardinalidad}
\begin{itemize}
\item Capítulo 11: Relaciones
\item Capítulo 12: Funciones
\item Capítulo 13: Demostraciones en el Cálculo
\item Capítulo 14: Cardinalidad de Conjuntos
\end{itemize}
\vspace{0.1in}

Estos capítulos finales se conciernen principalmente con la idea de las \textit{funciones}, las cuales son centrales en todas las matemáticas. Al dominar este material estarás listo para cursos avanzados de matemáticas tales como álgebra abstracta, análisis, topología, combinatoria y teoría de computación.

\clearpage
Los capítulos están organizados de la manera en que se muestra en el siguiente árbol de dependencia. La columna izquierda forma el núcleo del libro; cada capítulo en esta columna usa material de los capítulos más arriba. Los Capítulos 3 y 13 pueden omitirse sin perder la continuidad. Pero el material en el Capítulo 3 es una gran fuente de ejercicios, y el lector que lo omita debería ignorar los ejercicios posteriores que hacen uso del mismo. El Capítulo 10, sobre inducción, puede también omitirse sin afectar la continuidad. Sin embargo, la inducción es un tema que la mayoría de los cursos sobre demostraciones incluyen. 


\begin{figure}[!h]
\centering
Árbol de Dependencia

%\medskip
\dummypicture
\end{figure}


\textbf{Al instructor:} El libro está diseñado para un curso de tres o cuatro créditos. Un curso que enfatiza las matemáticas discretas podría cubrir los capítulos 1-12. Un curso que es mayormente una preparación para análisis podría cubrir todos los capítulos excepto el Capítulo 3. La siguiente tabla (para un semestre de catorce semanas) es un híbrido de estas dos opciones. Las secciones marcadas con un * pueden requerir solamente una breve mención en la clase, o pueden asignarse a los estudiantes.

\begin{center}
\begin{tabular}{|l|l|l|l|}
% Header
\hline
Semana & Lunes &  Miércoles & Viernes\\ 
\hline
\hline

% Main content
1 & Sección 1.1 & Sección 1.2 & Secciones 1.3,1.4 \\ \hline
2 & Secciones 1.5,1.6,1.7 & Sección 1.8 & Secciones 1.9*,2.1 \\ \hline
3 & Sección 2.2 & Secciones 2.3,2.4 & Secciones 2.5,2.6 \\ \hline
4 & Secciones 2.7 & Secciones 2.8*,2.9 & Secciones 2.10,2.11*,2.12* \\ \hline
5 & Secciones 3.1,3.2,3.3 & Secciones 3.4,3.5 & Secciones 3.5,3.6 \\ \hline
6 & EXAMEN & Secciones 4.1,4.2,4.3 & Secciones 4.3,4.4,4.5* \\ \hline
7 & Secciones 5.1,5.2,5.3* & Sección 6.1 & Secciones 6.2,6.3* \\ \hline
8 & Secciones 7.1,7.2*,7.3 & Secciones 8.1,8.2 & Sección 8.3 \\ \hline
9 & Sección 8.4 & Secciones 9.1,9.2,9.3* & Sección 10.1 \\ \hline
10 & Secciones 10.1,10.4* & Secciones 10.2,10.3 & EXAMEN \\ \hline
11 & Secciones 11.1,11.2 & Secciones 11.3,11.4 & Secciones 11.5,11.6 \\ \hline
12 & Sección 12.1 & Sección 12.2 & Sección 12.2\\ \hline
13 & Secciones 12.3,12.4* & Sección 12.5 & Secciones 12.5,12.6* \\ \hline
14 & Sección 14.1 & Sección 14.2 & Secciones 14.3,14.4* \\

\hline
\end{tabular}
\end{center}

El libro \textit{completo} podría cubrirse en un curso de 4 créditos, o en un curso de 3 créditos dirigido a una audiencia un poco más madura.

\vspace{0.1in}

\textbf{Reconocimientos.} Agradezco a mis estudiantes en los cursos MATH 300 en VCU por ofrecerme sus comentarios a medida que leían la primera edición de este libro. Gracias especialmente a Cory Colbert y Lauren Pace por eliminar errores tipográficos e inconsistencias. Estoy especialmente endeudado con Cory por leer los primeros borradores de cada capítulo y capturar numerosos errores antes de que publicara el borrador final en mi página web. Cory también creó el índice, sugirió algunos ejercicios interesantes, y proveyó algunas soluciones. Gracias a Moa Apagodu, Sean Cox, Brent Cody y Andy Lewis por sugerir algunas mejoras mientras utilizaban el libro para enseñar. Estoy endeudado con Lon Mitchell, cuyo conocimiento sobre composición tipográfica y publicación bajo demanda hizo la versión impresa de este libro una realidad.


Y gracias a los innumerables lectores alrededor del mundo quienes me contactaron para informarme sobre errores y omisiones. Debido a todos ustedes, este es un mejor libro.

